# Workshops 2022 | First Edition

## Linguistics — the base of Natural Language Processing

* Speaker: _Vlad Neacșu_

## Python & Data Visualisation

* Speaker: _Miruna Zăvelcă_
* [Dataset, Exercises](Python/)

## Machine Learning

* Speaker: _Antonio Bărbălău_
* [Datasets, Exercises](Machine%20Learning/)

## Natural Language Processing

* Speaker: _Matei Bejan_
* [Slides](Natural%20Language%20Processing/)

## Deep Learning

* Speaker: _Antonio Bărbălău_
* [Exercises](Deep%20Learning/)

## Contextual Word Embeddings & Intro to Transformers

* Speaker: _Andrei Manolache_
* [Presentation, Exercises](Transformers/)

## Language Models (Before They Were Cool)

* Speakers: _Angeliki Lazaridou & Elena Gribovskaya_
