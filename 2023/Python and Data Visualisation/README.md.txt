# Exerciții

1.1. Creează o funcție care primește o propoziție și o separă în funcție de un separator dat ca parametru (spațiu, punct etc).



1.2. Creează o funcție care primește o listă de cuvinte și returnează lista cuvintelor unice.



2. Creează un numpy array de 5x5 format din elemente aleatorii. Într-un alt numpy array copiază primele 4 coloane, apoi concatenează o coloană de zerouri. Schimbă primul element din al doilea array și adună-le.



3.1. Conectează acest Colab la contul tău Drive. Downloadează fișierul Lyrics-Genre.csv și adaugă-l în proiect. Citește datele ca un dataframe Pandas și afișează primele 10 linii.



3.2. Afișează câte datapoints (cântece per gen muzical) există pentru fiecare gen muzical folosind funcții din Pandas.



3.3. Creează următoarele coloane noi pentru dataframe-ul vostru: număr cuvinte, număr cuvinte unice, număr propoziții, densitatea medie a propozițiilor din datapoint (număr propoziții / număr cuvinte). Completează aceste informații.



3.4. Creează o listă cu numele coloanelor nou create. Folosește funcția _describe()_ din pandas pentru a vizualiza statisticile pe aceste coloane.



3.5. Calculează și afișează valorile medii și maxime pentru datele de mai sus în funcție de genul muzical.



4. Afișează câte cântece există pentru fiecare gen muzical (exercițiul 3.2) pe un barchart folosind matplotlib.



5. Creează un WordCloud folosind toate cuvintele dintr-un cântec oarecare din setul de date.



Alte exerciții și linkuri utile:
- https://fmi-unibuc-ia.github.io/ia/Laboratoare/Introducere-in-python.pdf
- https://fmi-unibuc-ia.github.io/ia/Laboratoare/Laboratorul%201.pdf
- https://matplotlib.org/stable/tutorials/introductory/usage.html
- https://www.freecodecamp.org/learn/scientific-computing-with-python/